package main

import (
	"net/http"
	"strings"
)

var endpoint404 = NewEndpoint404("")

type Mux struct {
	mux http.ServeMux

	wrapper func(Endpoint, http.ResponseWriter, *http.Request)
}

// NewMux return a new endpoint Mux.
// If provided, wrapper is expected to call whatever Endpoint it receives.
// It is useful for testing or loggin purposes.
func NewMux(wrapper func(Endpoint, http.ResponseWriter, *http.Request)) *Mux {
	return &Mux{
		http.ServeMux{},
		wrapper,
	}
}

func (self *Mux) Handle(e Endpoint) {
	p := e.Pattern()

	// An empty pattern or a domain without a trailing slash are considered invalid configurations
	// by Golang's http.ServeMux{}
	// All valid configurations will result in path passing, which might not be desired,
	// hence this workaround.
	if strings.IndexByte(p, '/') == -1 {
		p += "/"
	}

	self.mux.Handle(p, e)
}

func (self *Mux) Handler(r *http.Request) Endpoint {
	handler, pattern := self.mux.Handler(r)

	if pattern == "" {
		return endpoint404
	}

	endpoint, ok := handler.(Endpoint)
	if !ok {
		return endpoint404
	}

	// Workaround for non-path-passing root handlers
	if endpoint.Pattern() != pattern {
		if r.URL.Path != "/" {
			return endpoint404
		}
	}

	return endpoint
}

func (self *Mux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h := self.Handler(r)

	if self.wrapper != nil {
		self.wrapper(h, w, r)
	} else {
		h.ServeHTTP(w, r)
	}
}
