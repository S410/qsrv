package main

import (
	"io"
	"log"
	"net/http"
)

type EndpointProxy struct {
	endpointPrototype

	client  *http.Client
	handler http.Handler
}

func NewEndpointProxy(pattern, target string, client *http.Client) *EndpointProxy {
	ep := &EndpointProxy{
		endpointPrototype{
			pattern,
			target,
			"EndpointProxy<\"" + pattern + "\"<-\"" + target + "\">",
		},
		client,
		nil,
	}

	_, path := patternSplitHost(pattern)

	ep.handler = http.StripPrefix(
		path,
		http.HandlerFunc(ep.serveHTTP),
	)

	return ep
}

func (self *EndpointProxy) serveHTTP(w http.ResponseWriter, r *http.Request) {
	u := joinUrl(self.target, r.URL.Path)

	log.Println(u)

	pr, err := http.NewRequestWithContext(r.Context(), r.Method, u, r.Body)
	if err != nil {
		panic(err)
	}

	pr.Header = r.Header
	pr.Header.Add("X-Forwarded-For", r.Host)

	resp, err := self.client.Do(pr)
	if err != nil {
		panic(err)
	}

	wh := w.Header()
	for header := range resp.Header {
		wh.Set(header, resp.Header.Get(header))
	}

	w.WriteHeader(resp.StatusCode)

	io.Copy(w, resp.Body)
}

func (self *EndpointProxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	self.handler.ServeHTTP(w, r)
}
