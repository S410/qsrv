package main

import (
	"bufio"
	"errors"
	"fmt"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"unicode"
	"unicode/utf8"
)

const (
	arrowLeft   = "<-"
	arrowRight  = "->"
	arrowString = "<$"
)

var (
	regexpArrows = regexp.MustCompile(
		makeRegexpOr(
			arrowLeft,
			arrowRight,
			arrowString,
		),
	)

	regexpLineComment     = regexp.MustCompile(`^\s*#`)
	regexpTrailingComment = regexp.MustCompile(`\s+#`)

	errorFailedToSplitAtArrow = errors.New(
		"Failed to parse the rule! No <-, -> or <$?",
	)

	errorMalformedRule = errors.New(
		"Failed to parse the rule. Is it malformed?",
	)
)

func makeRegexpOr(s ...string) string {
	if len(s) == 0 {
		return ""
	}

	n := 0
	for _, x := range s {
		n += len(x)
	}
	// Assume and hope for a 25% increase in size due to escape and special characters
	n = (n * 5) >> 2

	b := strings.Builder{}
	b.Grow(n)

	thr := len(s) - 1
	b.WriteByte('(')
	for i, x := range s {
		b.WriteString(regexp.QuoteMeta(x))
		if i < thr {
			b.WriteByte('|')
		}
	}
	b.WriteByte(')')

	return b.String()
}

func splitLineAtArrow(line string) (left, right, arrow string, err error) {
	if ind := regexpArrows.FindStringIndex(line); ind != nil {
		return line[:ind[0]], line[ind[1]:], line[ind[0]:ind[1]], nil
	}

	err = fmt.Errorf("%s: %w", line, errorFailedToSplitAtArrow)

	return
}

func parseLine(line string) (Endpoint, error) {
	// Ignore commented out lines
	if regexpLineComment.MatchString(line) {
		return nil, nil
	}

	left, right, arrow, err := splitLineAtArrow(line)
	if err != nil {
		return nil, err
	}

	left = strings.TrimSpace(left)

	if arrow != arrowString {
		// Strip trailing comment, but only if it's not a string endpoint
		// String endpoints cannot have trailing comments
		if ind := regexpTrailingComment.FindStringIndex(right); ind != nil {
			right = right[:ind[0]]
		}

		// Strip all whitespaces in non-string endpoints
		right = strings.TrimSpace(right)
	} else {
		// Strip at most one leading whitespace if it's a string endpoint
		r, l := utf8.DecodeRuneInString(right)
		if unicode.IsSpace(r) {
			right = right[l:]
		}
	}

	switch arrow {
	case arrowString:
		return NewEndpointString(left, right), nil
	case arrowRight:
		return NewEndpointRedirect(left, right), nil
	case arrowLeft:
		// Are we a reverse proxy?
		u, err := url.ParseRequestURI(right)
		if err == nil {
			scheme := strings.ToLower(u.Scheme)
			if scheme == "http" || scheme == "https" {
				return NewEndpointProxy(left, right, client), nil
			}
		}

		// We're not a reverse proxy. Creating a filesystem endpoint now.
		p, err := filepath.Abs(right)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", line, err)
		}

		// filepath.Abs strips trailing /, so we need to restore it
		if strings.HasSuffix(right, "/") {
			p += "/"
		}

		return NewEndpointFs(left, p), nil
	}

	return nil, fmt.Errorf("%s: %w", line, errorMalformedRule)
}

func parseFile(p string) ([]Endpoint, error) {
	file, err := os.Open(p)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	endpoints := []Endpoint{}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		e, err := parseLine(line)
		if err != nil {
			logger.Println(line)
		} else if e != nil {
			endpoints = append(endpoints, e)
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return endpoints, nil
}
