package main

import (
	"net/http"
	"strings"
)

type EndpointFs struct {
	endpointPrototype
	recursive bool

	handler http.Handler
}

func NewEndpointFs(pattern, target string) *EndpointFs {
	isRecursive := strings.HasSuffix(target, "/")

	var handler http.Handler
	if isRecursive {
		handler = http.StripPrefix(
			pattern,
			http.FileServer(http.Dir(target)),
		)
	} else {
		handler = http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				http.ServeFile(w, r, target)
			},
		)
	}

	return &EndpointFs{
		endpointPrototype{
			pattern,
			target,
			"EndpointFs<\"" + pattern + "\"<-\"" + target + "\">",
		},
		isRecursive,
		handler,
	}
}

func (self *EndpointFs) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	self.handler.ServeHTTP(w, r)
}
