package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/tomasen/realip"
)

const (
	vt100escape = "\033["
	vt100orange = vt100escape + "33m"
	vt100bold   = vt100escape + "1m"
	vt100reset  = vt100escape + "0m"
)

var (
	logger    = log.New(os.Stderr, "", 0)
	clientLog = log.New(os.Stdout, "", log.Ldate|log.Ltime)

	endpointMux = NewMux(endpointLogger)

	server = &http.Server{
		IdleTimeout: 15 * time.Second,
		Handler:     http.Handler(endpointMux),
	}

	client = &http.Client{
		Timeout: time.Second * 30,
		Transport: &http.Transport{
			Dial: (&net.Dialer{
				Timeout: 5 * time.Second,
			}).Dial,
			TLSHandshakeTimeout: 5 * time.Second,
			Proxy:               http.ProxyFromEnvironment,
		},
	}

	errorFailedToBind = errors.New("Failed to bind")
)

func endpointLogger(ep Endpoint, w http.ResponseWriter, r *http.Request) {
	clientLog.Println(
		vt100orange,
		vt100bold,
		r.Method,
		r.Host+r.RequestURI,
		ep,
		vt100reset,
		"\n",
		vt100orange,
		realip.FromRequest(r),
		vt100reset,
		r.Header.Get("User-Agent"),
	)

	ep.ServeHTTP(w, r)
}

func intPortToAddr(i int) string {
	return ":" + strconv.Itoa(i)
}

func getOwnIps() ([]string, error) {
	var ips []string

	ifs, err := net.Interfaces()
	if err != nil {
		return nil, err
	}

	for _, i := range ifs {
		addrs, err := i.Addrs()
		if err != nil {
			return nil, err
		}

		for _, addr := range addrs {
			var ip net.IP

			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}

			if ip := ip.To4(); ip != nil {
				ips = append(ips, ip.String())
				continue
			}

			if ip := ip.To16(); ip != nil {
				ips = append(ips, "["+ip.String()+"]")
				continue
			}
		}
	}

	return ips, nil
}

var args = struct {
	// certificates
	useTLS  bool
	cert    string
	key     string
	certkey string

	// path to the config file
	config string

	// serving info
	addr       string
	validPorts []string

	// the port tryListen settles on
	port string
}{}

func parseArgs() {
	var (
		// Used for ReadTimeout and WriteTimeout
		timeout int

		// Split into "addr" and "port"
		address string
	)

	flag.IntVar(&timeout, "t", 0, "Read/write timeout")
	flag.StringVar(&address, "a", "", "Address")
	flag.StringVar(&args.cert, "c", "", "Certificate file")
	flag.StringVar(&args.key, "k", "", "Key file")
	flag.StringVar(
		&args.certkey, "ck", "",
		"Common certificate and key file prefix\n"+
			"-c and -k values are derived by appending .crt and .key to this value",
	)
	flag.StringVar(&args.config, "r", "",
		"File with rules.\n"+
			"/endpoint <- dir/file/url\n"+
			"/endpoint -> redirect_url\n"+
			"/endpoint <$ string",
	)

	flag.Parse()

	server.ReadTimeout = time.Duration(timeout)
	server.WriteTimeout = time.Duration(timeout)

	if args.certkey != "" {
		args.cert = args.certkey + ".crt"
		args.key = args.certkey + ".key"
	}

	if args.cert != "" && args.key != "" {
		args.useTLS = true
	}

	// Figure out address
	args.addr = address
	addr, port, err := net.SplitHostPort(address)
	if err == nil {
		args.addr = addr
	}

	// Figure out port(s) we can use
	if port != "" {
		args.validPorts = []string{port}
	} else {
		args.validPorts = make([]string, 0, 13)

		if args.useTLS {
			args.validPorts = append(args.validPorts, "443", "8443")
		} else {
			args.validPorts = append(args.validPorts, "80")
		}

		for port := 8080; port <= 8090; port++ {
			args.validPorts = append(args.validPorts, strconv.Itoa(port))
		}
	}
}

func parseEndpoints() (eps []Endpoint) {
	for _, x := range flag.Args() {
		r, err := parseLine(x)
		if err != nil {
			logger.Println(err)
		}
		if r != nil {
			eps = append(eps, r)
		}
	}

	if args.config != "" {
		e, err := parseFile(args.config)
		if err != nil {
			logger.Fatalln(err)
		}
		eps = append(eps, e...)
	}

	if len(eps) == 0 {
		rule, _ := parseLine("/" + arrowLeft + "./")
		if rule != nil {
			eps = append(eps, rule)
		}
	}

	return
}

func tryListen() (net.Listener, error) {
	for _, port := range args.validPorts {
		l, err := net.Listen("tcp", args.addr+":"+port)

		switch err := err.(type) {
		case *net.OpError:
			errorText := err.Err.Error()

			// Eat up non-fatal errors
			switch {
			case errorText == "bind: permission denied" && err.Op == "listen":
				logger.Printf("%s: permission denied", port)
				continue

			case errorText == "bind: address already in use":
				logger.Printf("%s: already in use", port)
				continue
			default:
				return nil, err
			}
		}

		args.port = port
		return l, nil
	}

	return nil, errorFailedToBind
}

func main() {
	parseArgs()

	eps := parseEndpoints()
	for _, ep := range eps {
		logger.Println(ep)

		endpointMux.Handle(ep)
	}

	l, err := tryListen()
	if err != nil {
		log.Fatalln(err)
	}

	// Log address(es) we're listening on
	proto := "http"
	if args.useTLS {
		proto = "https"
	}

	logger.Println("Listening on:")
	if args.addr == "" {
		ips, _ := getOwnIps()

		for _, ip := range ips {
			logger.Printf(" %s://%s:%s\n", proto, ip, args.port)
		}
	} else {
		host := fmt.Sprintf("%s://%s:%s", proto, args.addr, args.port)
		addr := fmt.Sprintf("%s://%s", proto, l.Addr().String())

		logger.Printf(" %s\n", host)
		if host != addr {
			logger.Printf(" %s\n", addr)
		}
	}

	if args.useTLS {
		err = server.ServeTLS(l, args.cert, args.key)
	} else {
		err = server.Serve(l)
	}

	log.Fatalln(err)
}
