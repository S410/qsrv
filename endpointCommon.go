package main

import (
	"net/http"
	"net/url"
	"strings"
)

type Endpoint interface {
	Pattern() string
	Target() string
	String() string
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}

type endpointPrototype struct {
	pattern string
	target  string

	asString string
}

func (self *endpointPrototype) Pattern() string {
	return self.pattern
}

func (self *endpointPrototype) Target() string {
	return self.target
}

func (self *endpointPrototype) String() string {
	return self.asString
}

func joinUrl(a, b string) string {
	a = strings.TrimRight(a, "/")
	b = strings.TrimLeft(b, "/")

	if b == "" {
		return a
	}

	return a + "/" + b
}

func changeHostString(u url.URL, host string) string {
	u.Host = host
	return u.String()
}

func patternSplitHost(pattern string) (host string, path string) {
	if len(pattern) == 0 {
		return
	}

	path = pattern

	if pattern[0] != '/' {
		index := strings.IndexByte(pattern, '/')
		if index == -1 {
			index = len(pattern)
		}

		host = pattern[:index]
		path = pattern[index:]
	}

	return
}
