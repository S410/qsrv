package main

import (
	"net/http"
	"strings"
)

type EndpointString struct {
	endpointPrototype

	data []byte
}

func NewEndpointString(pattern, data string) *EndpointString {
	return &EndpointString{
		endpointPrototype{
			pattern,
			"",
			"EndpointString<\"" + pattern + "\"<$\"" + data + "\">",
		},
		[]byte(strings.ReplaceAll(data, "\\n", "\n")),
	}
}

func (self *EndpointString) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write(self.data)
}

type Endpoint404 struct {
	endpointPrototype
}

func NewEndpoint404(pattern string) *Endpoint404 {
	return &Endpoint404{
		endpointPrototype{
			pattern,
			"",
			"Endpoint404<\"" + pattern + "\">",
		},
	}
}

func (self *Endpoint404) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	http.NotFound(w, r)
}
