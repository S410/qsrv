package main

import (
	"net/http"
	"strings"
)

type EndpointRedirect struct {
	endpointPrototype

	path string
}

func NewEndpointRedirect(pattern, target string) *EndpointRedirect {
	ep := &EndpointRedirect{
		endpointPrototype{
			pattern,
			target,
			"EndpointRedirect<\"" + pattern + "\"->\"" + target + "\">",
		},
		"",
	}

	_, ep.path = patternSplitHost(pattern)

	return ep
}
func (self *EndpointRedirect) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	http.Redirect(
		w, r,
		joinUrl(self.target, strings.TrimPrefix(r.URL.Path, self.path)),
		http.StatusMovedPermanently,
	)
}
